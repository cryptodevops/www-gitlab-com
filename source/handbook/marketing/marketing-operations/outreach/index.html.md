---
layout: handbook-page-toc
title: "Outreach"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Set-Up  
User set-up instructions can be found [here](https://docs.google.com/document/d/1VJpZaoDoGtwN4Dp21g_OuhVauN2prHNyUtmIWKuROUY/edit#heading=h.v6blhaepsusl).

### Sequences  
Sequences are one of the main features of Outreach. Users can create a series of touchpoints in order to communicate and automate their workflows.
We currently have two types of sequences. Master and personal. Master sequences are created to be shared and used across teams. Personal are for your own use.

Naming convention for master sequences include the following:

- MASTER
- LOW or HIGH touch
- Name of campaign
- Region or language (NORAM, APAC, EMEA or Spanish, French, Russian, ect)

Example: | **MASTER - HIGH - Just Commit - NORAM**

When creating a new master sequence please tag marketing ops for QA review prior to enabling the sequence. 
Personal sequences do not need to be reviewed unless you wish to have an additional pair of eyes on it.

### Sequence Settings
You may choose the best delivery schedule and ruleset applicable to your goals. 
You may choose whether or not others can see and use your sequence or if you would like to keep it private. 
All sequence must have Throttle's enabled. This helps to stagger the volume of prospects moving through a sequence at one time. 
It is necessary to help prevent users from hitting the email provider's mailing limits. 
If you have too much volume this may result in you being kicked out of your email inbox by our provider. 
**Max adds per user every 24 hours are to be set up to 75.** 
If you need to request special sequence settings please reach out to Marketing Operations with your use case.

### Collections
Collections are an easy way to group sequences, snippets, and templates that get assigned to user groups for easier access. 
Examples associated to our SDR groups include our `SDR Inbound 2019` and `Tanuki Tribe` collections. 
You may request new collections by opening an issue in the Marketing Operations Project.

### Rulesets
- **Default** - Does not make tasks on your behalf. Prospects can be added to these sequences more than once, if not already active. prospects are not able to be added to other exclusive sequences, but can be added to a non-exclusive sequence. Updates prospoects to appropriate stages. Resumes out of office prospects after 5 days.
- **Campaign** - Follows the same rules as the default settings except out of office are resumed 1 day after.
- **Create Call Task** - Follows same rules as the default except it will create call tasks on opened emails. Minimum email opens needed: 2
- **Event** - Prospects can only be added to this sequence once. They are not exclusive to this sequence. Out of office are resumed 1 day after.
