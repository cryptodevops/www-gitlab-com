---
layout: markdown_page
title: "Evangelist Program operations"
---

## Meetup platform

We are currently [evaluating our meetup platform of choice](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/1).

- Meetup platform ([currently evaluating](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/1))
- [Merchandise](merchandise.html)
