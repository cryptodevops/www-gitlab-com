---
layout: handbook-page-toc
title: "Gitaly Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

- Gitaly's [public issue tracker](https://gitlab.com/gitlab-org/gitaly/issues/).
- [Chat channel](https://gitlab.slack.com/archives/g_create_gitaly); please use
the `#g_create_gitaly` chat channel for questions that don't seem appropriate to
use the issue tracker for.

## What is the Gitaly team?

The Gitaly team is responsible for building and maintaining systems to ensure
that the git data storage tier of GitLab instances, and _GitLab.com in particular_,
is fast. Gitaly migrated GitLab.com away from NFS, and is now working on a
highly available Git storage layer.

### Development

The goals of Gitaly are

1. Deliver a reliable storage layer
1. Optimize git services using caching

See [the design document](https://gitlab.com/gitlab-org/gitaly/tree/master#reason) for an in-depth explanation behind the motivation for GitLab.
