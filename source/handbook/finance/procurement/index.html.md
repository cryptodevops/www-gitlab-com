---
layout: handbook-page-toc
title: "The GitLab Procurement Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}


# The GitLab Procurement Team is your Partner for the Procure to Pay Process

## Requesting Procurement Services
 
There are several third-party vendor processes that procurement supports, depending on your needs. Team members can purchase goods and services on behalf of the company in accordance with the Signature Authorization Matrix and guide to Spending Company Money. However, any purchases requiring contracts must first be reviewed by procurement, then signed off by a member of the executive team. Be sure to also check out our guide on Signing Legal Documents.

### 1. New Vendor Evaluation

If you have a new business need and are seeking a third party vendor to support it, you must review the market capabilities defined by your overall spend BEFORE selecting your vendor. 

*Note: Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s).*

| Estimated Spend | Market Review Needed |
| ------ | ------ |
| <$25K | None |
| >$25K and <$100K | Three Bids | 
| >$100K | RFP |

If there is a business reason why the market review is not necessary, please obtain a bid waiver from your functional leader and send to procurement for approval.

For instructions on how to evaluate a new vendor, please see the LINK COMING SOON!

### 2. Existing Vendor Negotiation (for Renewals and True-Ups)

If you have an existing vendor that needs a contract renewal or a true-up mid contract, work with procurement BEFORE agreeing to business terms. For all  contract renewals, **contact procurement 90-60 days before the existing contract expires**. Procurement will engage our software buying partner Vendr to ensure that the quoted pricing is competitive. Vendr can help negotiate directly with vendors on both new subscriptions and renewals. It is preferred that we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors. 

For instructions on how to submit an existing vendor negotiation, please see the LINK COMING SOON!

### 3. Vendor Contract Review

BEFORE agreeing to business terms, identify whether you need a New Vendor Market Review (#1 above), or an Existing Vendor Negotiation (#2 above). If you have completed one of those, you are ready to seek review of a vendor contract. Your request must go through GitLab's Vendor Management process to make sure it has been reviewed by appropriate stakeholders and that the request receives all necessary approvals.  This process involves Finance (for budgetary purposes), Security (when personal data is involved to ensure the vendor's systems and procedures meet our minimum standard), Business Operations, Procurement (for contract review), and an approved Signer (for final approval and signing).  

For instructions on how to submit a new vendor contract for review, please see the [Vendor and Contract Approval Workflow](/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow). Once the contract is completed it should be uploaded by the requestor into our contract management database tool [ContractWorks](/handbook/legal/vendor-contract-filing-process/).

### 4. Vendor Onboarding

As new (and existing) vendors will be supporting GitLab, we are implementing new tools such as Tipalti for onboarding and a PO Module for all payments. More info coming soon on how to use these.

### 5. Vendor Performance Issues

If there are performance and/or quality issues from an existing third-party vendor, procurement can support the resoluation and/or re-evaluation of a replacement vendor. More info coming soon.

### 6. All Other Procurement Requests 

Issue Tracking Coming Soon.
 
# Contract Templates
 
- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)
- [Data Processing Addendum](https://drive.google.com/file/d/1Of9hxDYpKDW4t9VF2Zay-lByZrxGx9DY/view?ts=5cdda534)
- [SaaS Addendum](https://drive.google.com/file/d/1NwaYid6qIJk9YscaRoY-uY5bEGsN1uu2/view?usp=sharing)

# General Topics and FAQs

## 1. What is the main objectives of Procurement?

At GitLab Procurement operates with three main goals:
1.  Cost Savings
2.  Centralize Spending
3.  Compliance

More FAQ's coming soon! Please submit relevant questions to @a.hansen.

# Important Pages Related to Procurement
 
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [General Guidelines](/handbook/general-guidelines/) - general company guidelines
* [Terms](/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Privacy Policy](/privacy/) - GitLab's policy for how it handles personal information 
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents


## Related Processes
 
- [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
