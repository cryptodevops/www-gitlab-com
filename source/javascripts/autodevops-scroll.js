(function() {
  var rightArrow = document.getElementById('right-arrow');
  var leftArrow = document.getElementById('left-arrow');
  var stepContainer = document.getElementById('step-container');
  var autoBuild = document.getElementById('auto-build');

  function scrollRight() {
    if ((stepContainer.scrollWidth - stepContainer.scrollLeft) - stepContainer.offsetWidth >= 0) {
      stepContainer.scrollLeft += autoBuild.offsetWidth + 40;
    }
  }

  function scrollLeft() {
    if (stepContainer.scrollLeft >= 0) {
      stepContainer.scrollLeft -= autoBuild.offsetWidth + 40;
    }
  }

  rightArrow.addEventListener('click', scrollRight);
  leftArrow.addEventListener('click', scrollLeft);
})();
