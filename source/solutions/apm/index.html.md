---
layout: markdown_page
title: "Integrated APM for Cloud Native Applications"
---

### Why is a DevOps Integrated APM solution essential?
Performance and availability mean everything to end users of applications. However, often monitoring has been separated from the development lifecycle - making triaging extremely hard. Without integrating application performance monitoring within the DevOps lifecycle, monitoring metrics are visualised independently of development objectives, release workflows and business metrics - resulting in longer time to remediate issues primarily due to lack of visibility into deployment changes, lack of a feedback loop from production environments into development and testing improvements.

With this in mind, GitLab believes that it is key to complete the loop in the DevOps lifecycle by offering an integrated APM solution that focuses on key areas of monitoring - user experience (roadmap), metrics, traces, and logs - and integrates with development processes and business outcomes.

### Observability for Cloud Native Applications
* Automatically enable monitoring for Kubernetes Clusters deployed from GitLab with traceability back to issues & code changes
* Collect and display performance metrics for supported Prometheus Exporters such as Kubernetes, NGINX, HAProxy & Amazon CloudWatch
* Extend supported environments through [Prometheus Exporter Ecosystem](https://prometheus.io/docs/instrumenting/exporters/) (Roadmap)
* View Performance Metrics without leaving the GitLab interface

### Distributed Tracing & Logging
* View Pods and Container Logs of connected Kubernetes clusters automatically within GitLab
* Track every function or microservice with distributed tracing powered by Jaeger
* Troubleshoot tracing results within GitLab without cross launching into other consoles

### Dashboarding and Alerting
* Configure Prometheus exporter as an input to Grafana and view application metrics without leaving GitLab
* Setup Prometheus alerts from the GitLab console and create issues automatically when an alert is received
* Consistent alerting experience by integrating Prometheus alerts into GitLab alerts

### User Experience
* Synthetic User Experience Monitoring is part of our [Roadmap](/direction/monitor/)

### APM Demo

Here is a complete monitor demo (TBD: To replace with APM only demo)
<iframe width="560" height="315" src="https://www.youtube.com/watch?v=mm_8wVjn808" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Help and more information
* [APM Vision & Roadmap](/direction/monitor/)
* [Documentation](https://docs.gitlab.com/ee/user/project/operations/)
