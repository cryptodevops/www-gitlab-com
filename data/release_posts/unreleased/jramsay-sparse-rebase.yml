features:
  secondary:
    - name: "Faster rebases using sparse checkout"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html'
      image_url: 'images/unreleased/sparse-rebase.png'
      reporter: jramsay
      stage: create
      issue_url: 'https://gitlab.com/gitlab-org/gitaly/issues/1623'
      description: |
        Fast forward and semi-linear merge methods require that there are no
        changes in the target branch that are not in the source branch. When
        there are changes in the target branch that are not in the source
        branch, a **Rebase** button is shown to bring the merge request up to
        date.

        When Git performs a rebase, a worktree is used to complete the
        operation. In GitLab 12.5, a sparse checkout is used when creating the
        worktree, which means that the worktree used to perform the rebase
        doesn't include the full working copy of a repository, but just a few
        files. Previously, checking out the full worktree would take an order
        of magnitude longer than the rebase. On GitLab.com we have observed an
        80% reduction in median rebase duration.
